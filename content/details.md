### Final Exam Graded papers

The final examination has been graded. Please check your answer papers, totalling and grade entries at one of the following times:

* __Dates:__ Monday, December 2, 2019 and Tuesday, December 3, 2020.
* __Time:__ From 8:30 am to 12:15 pm.

As I will enter the grades in the system on Wednesday, December 3, 2019, please ensure that you check everything during these times.


<section>
  <h4>Midterm Examination</h4>
  <ul>
    <li><strong>Date:</strong> Thursday, September 26, 2019. </li>
    <li><strong>Time:</strong> 8:30 am - 10:30 am.</li>
    <li><strong>Venue:</strong> LH-5, Department of Mathematics.</li>
  </ul>
</section>

<section>
  <h4>Midterm Examination II (Make-up)</h4>
  <p> This examination is worth $20$ marks. The composite midterm score out of $40$ will be the maximum of the $(\text{midterm 1 score})$ and  $(\frac{\text{midterm 1 score}}{2} + \text{midterm 2 score})$.</p>
  <ul>
    <li><strong>Date:</strong> Thursday, October 24, 2019. </li>
    <li><strong>Time:</strong> 8:30 am - 9:30 am.</li>
    <li><strong>Venue:</strong> LH-5, Department of Mathematics.</li>
  </ul>
</section>

<section>
  <h4>Final Examination</h4>
  <ul>
    <li><strong>Date:</strong> Wednesday, November 27, 2019. </li>
    <li><strong>Time:</strong> 2:00 pm - 5:00 pm.</li>
    <li><strong>Venue:</strong> LH-5, Department of Mathematics.</li>
  </ul>
</section>


<section>


  <h4> Course Details</h4>
  <ul>
    <li><strong>Instructor:</strong> <a href="http://math.iisc.ac.in/~gadgil" target="_blank"> Siddhartha Gadgil</a>
    </li>
    <li><strong>E-mail:</strong> <a href="mailto:siddhartha.gadgil@gmail.com" target="_blank">
        siddhartha.gadgil@gmail.com</a>.</li>
    <li> <strong>Office:</strong> N-15, Department of Mathematics. </li>
    <li><strong>Timing: </strong>
      Tuesday, Thursday 8:30 am to 10:00 am.
    </li>
    <li> <strong>Teaching Assistant:</strong> Pranab Sarkar (<a href="mailto:pranabsarkar@iisc.ac.in">pranabsarkar@iisc.ac.in</a>) </li>
    <li><strong>Lecture Venue: </strong> LH-5, Department of Mathematics, IISc.</li>
    
  </ul>

</section>
