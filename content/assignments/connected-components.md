---
title: "Connected Components"
date: 2019-08-20
---

In the following you may use basic properties of connected sets and continuous functions.

1. Let $X$ be a topological space. Show that $X$ can be _uniquely_ expressed as the _disjoint union_
   `$$X = \coprod_{\alpha\in A} X_\alpha$$`
   such that for each `$\alpha\in A$`, `$X_\alpha\subset X$` is non-empty and connected and such that if $Z\subset X$ is connected then $Z \subset X_\alpha$ for some $\alpha\in A$. We call the sets `$X_\alpha$` the connected components of $X$.
2. For a topological space $X$, define $F(X)$ to be the _set_ of connected components of $X$. For $X$ and $Y$ topological spaces and $f: X\to Y$
    a continuous function from $X$ to $Y$, show that there is a well-defined function $f_* = F(f): F(X)\to F(Y)$.
3. Show that $F$ defined as above is a _functor_ from topological spaces to sets, i.e. compositions and identities are respected.
4. Let $X$ be a connected topological space and $A\subset X$ a subspace that is not connected. What can you say about the cardinalities of $F(X)$ and $F(A)$?
5. Let $X$ be a connected topological space and $A\subset X$ a subspace that is not connected. Using (3) and (4) above (but no other properties of connectivity), show that there is no continuous function $r: X \to A$ such that, for $a \in A$, $r(a) = a$.
   

