---
title: Homotopy and Fundamental Groups
date: 2019-09-17
---

Most of the following problems are from Allen Hatcher's _Algebraic Topology_, specifically the [online version](http://pi.math.cornell.edu/~hatcher/AT/AT.pdf).

1. Hatcher, Chapter 0, Problem 4.
2. Hatcher, Chapter 0, Problem 5.
3. Hatcher, Chapter 0, Problem 6.
4. Hatcher, Chapter 1, Problem 5.
5. Hatcher, Chapter 1, Problem 7.
6. Let `$(X, x_0)$` and `$(Y, y_0)$` be based spaces and let `$p_x : X \times Y\to X$` and `$p_y: (X, Y) \to Y$` be the projection maps. Show that we have
   an _isomorphism_ `$\varphi: \pi_1(X\times Y, (x_0, y_0))\to \pi_1(X, x_0) \times \pi_1(Y, y_0)$` given by `$\varphi(\xi) = ((p_x)_*(\xi), (p_y)_*(\xi))$`.
7. Show that the M&ouml;bius band does not retract onto its boundary circle.
